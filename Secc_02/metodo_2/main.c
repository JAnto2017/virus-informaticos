/**
 * @file main.c
 * @author your name (you@domain.com)
 * @brief 
 * @version 0.1
 * @date 2024-06-15
 * 
 * @copyright Copyright (c) 2024
 * 
 * Mouse Simple Posición del Mouse para saturar la RAM y el Procesador
 */

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <conio.h>
#include <time.h>

int main(int argc, char const *argv[])
{
    while (1)
    {
        POINT mouse;            //estructura que tiene las coordenadas del mouse
        HDC _hdc = GetDC(NULL); //recupera un identificador para un contexto de dispositivo (DC)
                                //para el área del clinete de una ventana específica p para toda la pantalla
                                //visita: https://docs.microsoft.com/en-us/windows/desktop/api/winuser/nf-winuser-getdc
        
        if (_hdc)               // Si la variable _hdc se cumple entra y muestra posicion
        {
            GetCursorPos(&mouse);
            printf("Posición del mouse es %d ; %d\n", mouse.x, mouse.y);
        }        
    }
    
    return 0;
}
