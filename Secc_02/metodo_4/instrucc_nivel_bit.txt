Instrucciones a nivel de Bit



A veces no queremos operar con bytes completos, sino quizá poner a 1 un sólo bit, a 0, o comprobar en qué estado se encuentra. Bien que esto se puede hacer utilizando instrucciones como el AND (por ejemplo, si hacemos AND AX,1 y el primer bit de AX no está activado el flag de Zero se activará, y no así en otro caso), OR (los bits a 1 de la cifra con la que hagamos el OR activarán los correspondientes del origen y un 0 hará que nada varíe), y de nuevo AND para poner a cero (los bits del operando con el AND puestos a 1 no variarán, y los 0 se harán 0...).



El caso, que tenemos instrucciones específicas para jugar con bits que nos pueden ser útiles según en qué ocasiones (un ejemplo de utilización se puede ver en el código de encriptación de mi virus Unreal, presente en 29A#4). Estas son:



- BTS: Bit Test and Set, el primer operando indica una dirección de memoria y el segundo un desplazamiento en bits respecto a esta. El bit que toque será comprobado; si es un 1, se activa el carry flag (comprobable con JC, ya se sabe) y si es 0, pues no. Además, cambia el bit, fuera cual fuese en principio, a un 1 en la localización de memoria indicada. Por ejemplo, un "BTS [eax+21],16h" contaría 16h bits desde la dirección



"eax+21", comprobaría el bit y lo cambiaría por un 1.

- BTR: Bit Test and Reset, como antes el primer operando se refiere a una dirección y el segundo a un



desplazamiento. Hace lo mismo que el BTS, excepto que cambia el bit indicado, sea cual sea, por un 0.



- BT: Bit Test, hace lo mismo que las dos anteriores pero sin cambiar nada, sólo se dedica a comprobar y



cambiar el Carry Flag.



- BSWAP: Pues eso, Bit Swap... lo que hace es ver el desplazamiento con el segundo operando (el formato,



como en las anteriores), y cambiar el bit; si era un 1 ahora será un 0 y viceversa.



- BSF: Bit Scan Forward, aquí varía un poco el tema; se busca a partir de la dirección indicada por el segundo



operando para ver cuál es el bit menos significativo que está a 1. Si al menos se encuentra uno, su



desplazamiento se almacenará en el primer operando. Es decir, si hacemos BSF EAX,[EBX] y en EBX



tenemos la cadena 000001xxx, EAX valdrá 5 tras ejecutar esta instrucción. Tenemos también una instrucción,



BSR, que hace lo mismo pero buscando hacia atrás (Bit Scan Reverse)

