El programa escrito a continuación, imita una operación de multiplicación utilizando

tan sólo la suma, resolviéndolo mediante el algoritmo de que N * M es lo mismo que (N+N+N+...+N), M veces:

; Suponemos que EAX contiene N, y EBX, contiene M.

xor edx,edx ; Aquí vamos a almacenar el resultado final. La operación xor edx,edx hace EDX

= 0.

LoopSuma: ; Esto, es una etiqueta

add edx,eax ; A EDX, que contendrá el resultado final, le sumamos el primer multiplicando

dec ebx

jnz LoopSuma ; Si el resultado de decrementar el multiplicando EBX es cero, no sigue

sumando el factor de EAX.





Un programa tan sencillo como este, nos dará en EDX el producto de EAX y EBX. Veamos uno análogo para

la división:

; Suponemos que EAX contiene el dividendo y EBX el resto.

xor ecx,ecx ; ecx contendrá el cociente de la división

xor edx,edx ; edx va a contener el resto de la división

RepiteDivision:

inc ecx ; incrementamos en 1 el valor del cociente que queremos obtener

sub eax,ebx ; al dividendo le restamos el valor del divisor

cmp eax,ebx ; comparamos dividendo y divisor

jna RepiteDivision ; si el divisor es mayor que el dividendo, ya hemos acabado de ver el cociente

mov edx,eax

Se ve desde lejos que este programa es muy optimizable; el resto quedaba en EAX, con lo que a no ser que

por algún motivo en particular lo necesitemos en EDX, podríamos prescindir de la última línea y hacer que el

cociente residiera en ECX mientras que el resto sigue en EAX. También sería inútil, la línea "xor edx,edx" que

pone EDX a cero, dado que luego es afectado por un "mov edx,eax" y da igual lo que hubiera en EDX.

Hemos visto, además, cómo hacer un bucle mediante el decremento de una variable y su comprobación de si

llega a cero, y en el segundo caso, mediante la comprobación entre dos registros; para el primer caso vamos

a tener en el ensamblador del PC un método mucho más sencillo utilizando ECX como contador como va a

ser el uso de la instrucción LOOP, que veremos más adelante, y que es bastante más optimizado que este

decremento de a uno.