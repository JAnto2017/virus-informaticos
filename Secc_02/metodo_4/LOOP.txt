LOOP



La instrucción LOOP es un poco como la forma "oficial" de hacer bucles en ensamblador, y el porqué de que ECX sea considerado como "el contador". Este comando tiene un sólo parámetro, una posición de memoria (que normalmente escribiremos como una etiqueta). Cuando se ejecuta comprueba el valor de ECX; si es cero no sucede nada, pero si es mayor que cero lo decrementará en 1 y saltará a la dirección apuntada por su operando.



mov ecx, 10h ; Queremos que se repita 10h veces.



Bucle:



<codigo bucle>



<codigo bucle>



loop Bucle



Como es lógico, el loop actuará ejecutando lo que hay entre "Bucle" y él 10h veces, cada vez que llegue al



LOOP decrementando ECX en uno.

