# Tutorial para entrar a la Bios y qué hacer si no puedes

Por medio de este pequeño tutorial enseñaremos como entrar en la Bios de cualquier ordenador o portátil, y una alternativa por si la primera propuesta no funciona o no es válida para tu equipo.

La importancia de la Bios proviene de que nada más entrar en ella podemos configurar opciones importantes del equipo. Como la definición de aspectos tan básicos como la carga de Gnu Linux, Windows o Mac o del hardware.

Para entrar a la Bios has de:

1. Reinicia tu ordenador o portátil o enciéndelo.
2. Verás una de estas dos cosas mientras arranca tu ordenador: un logotipo del fabricante o un texto que pasa muy deprisa (normalmente por el borde inferior de la pantalla).

En la cual localizaras una de estas líneas de texto:

> Press [tecla] to run Setup
> Hit [tecla] to run Setup
> Press [tecla] to enter Setup
> Press [tecla] for SETUP
> [tecla]= Setup o Setup= [tecla]
> Press [tecla] to enter BIOS Setup
> Press [tecla] to access system configuration

Las teclas o shortcuts más típicos que permiten el acceso a la BIOS durante el arranque son:

> **Del** o **Supr**.
> **F2** (habitual en laptops)
> **F1**
> **F10**
> **Ctrl+Alt+Esc** (pulsar a la vez Ctrl y Alt y luego, sin soltarlas, Esc)
> **Ctrl+Alt+Enter**

3. Cuando sepas la tecla correcta, púlsala al arrancar el ordenador, nada más aparecer: Press [tecla] to run Setup o el texto que corresponda.

¡Recuerda! que si tu sistema operativo ( Gnu Linux, Mac o Windows) empieza a cargarse ya no podrás entrar y deberás reiniciar el ordenador para volver a intentarlo.

## No puedo entrar en la Bios, ¿Qué hago?

Tu solución dependerá de cuál sea tu problema. A continuación resolveremos los más comunes:

* No me da tiempo a leer los mensajes de la BIOS o del logotipo, lo cual hace que no descubras la tecla que debes pulsar.

### SOLUCIÓN 1

Pulsa la tecla Pause/Break justo después de arrancar el ordenador. La encontrarás en la esquina superior derecha del teclado.
Para reanudar el arranque después de haberlo pausado, pulsa cualquier otra tecla.
Puedes pausar/reanudar varias veces.

### SOLUCIÓN 2

Si sigue sin darte tiempo a leer, lo más fácil es buscar en Google un texto como “entrar en BIOS *(modelo de tu laptop o de tu placa base)*”. Es probable que encuentres el modo de hacerlo en tu caso concreto.

– Pulso la tecla correcta pero no funciona.

Los teclados inalámbricos pueden no funcionar durante el arranque del ordenador. Si no ocurre nada por más que pulses la tecla correcta es probable que ese sea tu caso.

### SOLUCIÓN 3

Consigue un teclado con conector mini-DIN (también se llama PS/2), enchúfalo a tu PC en lugar del teclado USB e inténtalo de nuevo.

– Mi ordenador me pide una contraseña para acceder a la BIOS.

### SOLUCIÓN 4

Algunas BIOS piden contraseña aunque no esté definida. Si lo hace la tuya, te bastará pulsa la tecla Intro para poder entrar.

### SOLUCIÓN 5

Cuando la BIOS tiene de verdad una contraseña, tendrás que recordarla si quieres tener acceso a ella.

¿No recuerdas la contraseña o no la has puesto tú? Prueba alguna de estas contraseñas genéricas (en inglés). Recuerda pulsar Intro cada vez que pruebes una.

### SOLUCIÓN 6

Hay varios métodos para resetear la BIOS y eliminar la contraseña que la protege. Implican acceder a la placa base y tienen sus riesgos. Esta es una opción poco recomendada, puesto que requiere de un usuario avanzado o de un técnico para realizar el proceso.

Los pasos para resetear la BIOS serian:
– Descubre tu modelo de placa.
– Busca el manual de ese modelo en el sitio web de su fabricante. Debe incluir instrucciones sobre cómo resetear la BIOS.

*Ten MUCHO cuidado con lo que tocas en la placa.

– Veo un logotipo en el arranque pero no aparece ningún texto.

Esto es muy inusual pero no imposible.

### SOLUCIÓN 7

Podrás ver los mensajes de la BIOS pulsando la tecla Esc o bien la tecla Tab (la que tiene dos flechas y está sobre el bloqueo de mayúsculas).

Si no funciona ninguna de las dos, haz una búsqueda en Internet para tu modelo de PC o de placa base, o consulta en el manual de ésta.

Para salir de la Bios:

* Usa la flecha derecha o la flecha izquierda de tu teclado para ir a la pestaña Exit.
* En general hay cuatro opciones en ella. Puedes pasar de una a otra con la flecha arriba o la flecha abajo:
* Exit & Save Changes. Para salir guardando los cambios que hayas hecho en las configuraciones.
* Exit & Discard Changes. Salir sin guardar tus modificaciones.
* Discard Changes. Desecha los cambios que hayas hecho pero manteniendo la BIOS abierta.
* Load Setup Defaults. Es una opción avanzada. No debes usarla en condiciones normales. Hace que la BIOS recupere sus configuraciones de fábrica.

La opción por defecto y que conviene utilizar en general es Exit & Save Changes. Ve hasta ella si no está ya seleccionada y pulsa la tecla Intro.

La BIOS te pedirá confirmación. Asegúrate de que OK está resaltado y pulsa Intro otra vez. Eso reiniciará el PC.